import {ICalEvent, IJSEvent} from "./types";
import {convertEventCSS, getCurrentTime, getWeekNames} from "./helper";

const TIME: string[] = [];
for (let i = 1; i <= 23; i++) {
    TIME.push(`${i}:00`)
}
const monthNames = ["January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December"
];
const weekNames = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];

const TIME_REFRESH_INTERVAL = 10 * 60 * 1000; // 10 minute
const MILI_SEC_IN_HOUR = 60 * 60 * 1000;

const eventThemes = {
    theme1: {
        label: "Purple",
        style: {
            backgroundColor: '#d7dbef',
            borderColor: '#bcc3e5',
        }
    },
    theme2: {
        label: 'Cyan',
        style: {
            backgroundColor: '#b3e1f7',
            borderColor: '#81cdf2'
        }
    },
}

const demoHoliday: () => ICalEvent[] = () => {
    const currentTime = getCurrentTime(new Date().getTime());
    const weekFirstDay = getWeekNames(new Date()).first + 3 * MILI_SEC_IN_HOUR;
    return [{
        text: 'Sprint Meeting',
        theme: eventThemes.theme1,
        timeFrom: weekFirstDay + 3 * MILI_SEC_IN_HOUR,
        timeTo: weekFirstDay + 9 * MILI_SEC_IN_HOUR,
    }, {
        text: 'Sprint Grooming',
        theme: eventThemes.theme1,
        timeFrom: weekFirstDay + (24 + 6) * MILI_SEC_IN_HOUR,
        timeTo: weekFirstDay + (24 + 12) * MILI_SEC_IN_HOUR,
    }].map(e => convertEventCSS(e, currentTime) as ICalEvent)
}

export {
    TIME,
    monthNames,
    weekNames,
    MILI_SEC_IN_HOUR,
    TIME_REFRESH_INTERVAL,
    eventThemes,
    demoHoliday,
}
