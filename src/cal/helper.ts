import {MILI_SEC_IN_HOUR, monthNames, weekNames} from './Constants'
import {ICalEvent, ICurrentTime, IJSEvent, ITimeFrame} from "./types";

const dayToMS = (day: number) => (day * 24 * 60 * 60 * 1000);
const AddZero = (num: number, redix?: number, str?: string) => {
    let  l= (String(redix || 10).length - String(num).length) + 1;
    return l> 0? new Array(l).join(str || '0') + num : num;
}
const getWeekNames = (date: Date) => {
    const dt = new Date(date.getTime())
    const first = dt.getDate() - dt.getDay(); // First day is the day of the month - the day of the week
    const last = first + 6; // last day is the first day + 6
    const currDate = new Date(dt.setDate(first));
    const week = [];
    for (let i = 0; i <=6; i++) {
        const day = currDate.getDate();
        week.push({
            isCurrent: day === date.getDate(),
            name: `${weekNames[i]} ${day}`,
            day
        })
        currDate.setDate(day+1)
    }
    const f1 = new Date(dt.setDate(first));
    f1.setHours(0,0,0,0);
    const f2 = new Date(dt.setDate(last));
    f2.setHours(23,59,59,999);
    return {
        week,
        first: f1.getTime(),
        last: f2.getTime(),
    }
}
const getCurrentTime = (dt: number) => {
    const curr = new Date().getTime();
    const d = new Date(dt);
    const month = monthNames[d.getMonth()];
    const year = d.getFullYear();
    const date = d.getDate()
    const min = d.getMinutes()
    const week = getWeekNames(d);
    const display = (week.first < curr) && (week.last > curr) ? 'block' : 'none'
    return {
        gridColumn: 7,
        gridRow: 10,
        top: `calc(${Math.floor((min / 60) * 100)}% - 1px)`,
        month,
        date,
        year,
        dt,
        display,
        ...week
    }
}
const convertEventCSS: (p1: IJSEvent, p2: ICurrentTime) =>  ICalEvent | null = (
        { text, theme, timeFrom, timeTo},
        {first, last, week}
    ) => {
    if ((timeFrom < first) || (timeTo > last)) {
        return null
    }
    const evFrom = new Date(timeFrom);
    const evTo = new Date(timeTo);
    const eventDate = evFrom.getDate();
    const span = Math.round((evTo.getTime() - evFrom.getTime()) / MILI_SEC_IN_HOUR)
    return {
        theme,
        text,
        style: {
            gridColumn: week.findIndex((w => w.day === eventDate)) + 3,
            gridRow: `${evFrom.getHours()} / span ${span}`
        },
        timeFrame: {timeFrom, timeTo}
    }
}
const getEventDisplay = ({timeFrom, timeTo}: ITimeFrame, {first, last}: ICurrentTime) => {
    if ((timeFrom < first) || (timeTo > last)) {
        return 'none'
    }
    return 'block'
}
const dateToHTML = (date: number | string) => {
    const d = new Date(date);
    // 2020-11-10T10:23
    return {
        date: [
            d.getFullYear(),
            AddZero(d.getMonth() + 1),
            AddZero(d.getDate())
        ].join('-'),
        time: `${AddZero(d.getHours())}:00`
    }
}
export {
    getWeekNames,
    dateToHTML,
    getCurrentTime,
    dayToMS,
    convertEventCSS,
    getEventDisplay
}
