import React from 'react';
import { render, screen } from '@testing-library/react';
import {Cal} from './Cal';

test('renders learn react link', () => {
  render(<Cal />);
  const linkElement = screen.getByText(/learn react/i);
  expect(linkElement).toBeInTheDocument();
});
