import React, {useReducer, useEffect} from 'react';
import './Cal.css';
import {TIME, weekNames, TIME_REFRESH_INTERVAL, demoHoliday} from './Constants'
import cns from 'classnames'
import { Button } from '@material-ui/core'
import {convertEventCSS, dayToMS, getCurrentTime, getEventDisplay} from "./helper";
import {ICalEvent, ICurrentTime, IJSEvent} from "./types";
import { EventPopUp } from "./EventPopUp";
import {defaultStateReducer} from "../common/helper";
import AddIcon from '@material-ui/icons/Add';
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos';
import SearchIcon from '@material-ui/icons/Search';
import MoreVertIcon from '@material-ui/icons/MoreVert';

interface IState {
  isPopUpOpen: boolean
  CalEvents: ICalEvent[]
  currentTime: ICurrentTime
}

const initialState = (): IState => {
  const currentTime = getCurrentTime(new Date().getTime());
  return {
    isPopUpOpen: false,
    CalEvents: [...demoHoliday()],
    currentTime
  }
}
let intervalRef: NodeJS.Timeout;
function Cal() {
  const [state, dispatch] = useReducer(defaultStateReducer, initialState());
  const {isPopUpOpen, CalEvents, currentTime} = state as IState;
  const changeDate = (ms: number, totMS?: number) => dispatch({
    type: 'change_time',
    payload: {
      currentTime: getCurrentTime(totMS || (currentTime.dt + ms))
    }
  })
  useEffect(() => {
    clearInterval(intervalRef)
    intervalRef = setInterval(() => changeDate(TIME_REFRESH_INTERVAL), TIME_REFRESH_INTERVAL);
    return () => clearInterval(intervalRef)
  }, []);
  const togglePopUp = (open: boolean) => {
    dispatch({
      type: 'change_event',
      payload: {
        isPopUpOpen: open
      }
    })
  }
  const onNewEvent = (ev: IJSEvent) => {
    const newEvents = CalEvents || [];
    const newEvent = convertEventCSS(ev, currentTime);
    console.log({ev, newEvent})
    newEvent && newEvents.push(newEvent)
    togglePopUp(false);
    newEvent && dispatch({
      payload: {
        CalEvents: newEvents
      },
      type: 'Add_Event'
    })
  }
  return (
      <div className="container">
        <div className="title">
          <Button variant="contained" onClick={() => togglePopUp(true)} color="primary">
            <AddIcon fill="#fff" />
          </Button>
          <Button variant="text" onClick={() => changeDate(-1 * dayToMS(7))}>
            <ArrowBackIosIcon htmlColor="#fff" />
          </Button>
          <Button variant="text" onClick={() => changeDate(dayToMS(7))}>
            <ArrowForwardIosIcon htmlColor="#fff" />
          </Button>
          <Button style={{color: '#fff'}} variant="text" onClick={() => changeDate(0, new Date().getTime())}>
            Today
          </Button>
          <div className="date">{`${currentTime.month} ${currentTime.year}`}</div>
        </div>
        <div className="days">
          <div className="filler"><SearchIcon /></div>
          <div className="filler"><MoreVertIcon /></div>
          {
            currentTime.week.map((w, index) => <div key={index} className={cns({
              day: true,
              current: w.isCurrent
            })}>{w.name}</div>)
          }
        </div>
        <div className="content">
          {
            TIME.map((t, ind) => (<div key={ind} className="time" style={{gridRow: ind + 1}}>{t}</div>))
          }
          <div className="filler-col"></div>
          {
            weekNames.map((t, ind) => (<div key={ind} className={cns({col: true, weekend: [0,6].includes(ind)})} style={{gridColumn: ind + 3}} />))
          }
          {
            TIME.map((t, ind) => (<div key={ind} className="row" style={{gridRow: ind + 1}} />))
          }
          {
            CalEvents.map(({theme, style, text, timeFrame}: ICalEvent, ind: number) => (<div key={ind} style={{...style, ...theme.style, display: getEventDisplay(timeFrame, currentTime)}} className="event">{text}</div>))
          }
          <div className="current-time" style={currentTime}>
            <div className="circle"></div>
          </div>
        </div>
        <EventPopUp onSubmit={onNewEvent} open={isPopUpOpen}/>
      </div>
  );
}

export {Cal};
