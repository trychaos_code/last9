import React, {useReducer} from "react";
import { TextField, RadioGroup, FormControl, FormLabel, FormControlLabel, Radio } from '@material-ui/core'
import {IJSEvent} from "./types";
import {eventThemes, MILI_SEC_IN_HOUR} from "./Constants";
import { FormPopUp } from "../common/design";
import {IEvent} from "../common/types";
import { dateToHTML} from "./helper";
import {defaultStateReducer} from "../common/helper";
import {makeStyles, Theme} from "@material-ui/core/styles";

const useStyles = makeStyles((theme: Theme) => ({
    formFields: {
        marginBottom: 25
    },
    notes: {
        color: 'rgba(255,0,0,0.5)',
        fontSize: 12
    }
}))
const initialState = () => {
    const currDt = new Date().getTime()
    const date = dateToHTML(currDt + MILI_SEC_IN_HOUR).date
    const timeFrom = dateToHTML(currDt + MILI_SEC_IN_HOUR).time
    const timeTo = dateToHTML(currDt + 3 * MILI_SEC_IN_HOUR).time
    return {
        date,
        timeTo,
        timeFrom,
        theme: eventThemes.theme1,
        text: "Event Name"
    }
}
interface IProps {
    open: boolean
    onSubmit: (p: IJSEvent) => void
}
const EventPopUp = ({onSubmit, open}: IProps) => {
    const classes = useStyles()
    const [state, dispatch] = useReducer(defaultStateReducer, initialState());
    const {date, timeTo, timeFrom, theme, text} = state;
    const handleChange = (ev: IEvent, field: string) => {
        let val = ev.target.value as any;
        if (field === 'theme') {
            val = Object.values(eventThemes).find(t => (t.label === val)) || eventThemes.theme1
        }
        dispatch({
            payload: {
                [field]: val
            },
            type: 'change_field'
        })
    }
    const onClose = () => {
        const evField = {text, theme, timeTo: new Date(`${date}T${timeTo}`).getTime(),timeFrom: new Date(`${date}T${timeFrom}`).getTime() };
        onSubmit(evField)
    }
    return <FormPopUp onCancel={onClose} onClose={onClose} onSubmit={onClose} open={open} title="Add New Event" subTitle={<div className={classes.notes}><sup>*</sup>All fields are required</div>}>
        <TextField
            className={classes.formFields}
            label="Event Name"
            type="text"
            defaultValue={timeFrom}
            InputLabelProps={{
                shrink: true,
            }}
            value={text}
            onChange={(ev) => handleChange(ev, 'text')}
        />
        <FormControl component="fieldset">
            <FormLabel component="legend">Theme</FormLabel>
            <RadioGroup
                className={classes.formFields}
                aria-label="theme"
                name="theme"
                value={theme.label}
                onChange={(ev) => handleChange(ev, 'theme')}
            >
                {
                    Object.values(eventThemes).map((t, ind) => <FormControlLabel key={ind} value={t.label} control={<Radio />} label={t.label} />)
                }
            </RadioGroup>
        </FormControl>
        <TextField
            className={classes.formFields}
            label="Date"
            type="date"
            defaultValue={timeFrom}
            InputLabelProps={{
                shrink: true,
            }}
            value={date}
            onChange={(ev) => handleChange(ev, 'date')}
        />
        <TextField
            className={classes.formFields}
            label="Time From"
            type="time"
            defaultValue={timeFrom}
            InputLabelProps={{
                shrink: true,
            }}
            value={timeFrom}
            onChange={(ev) => handleChange(ev, 'timeFrom')}
        />
        <TextField
            className={classes.formFields}
            label="Time To"
            type="time"
            defaultValue={timeFrom}
            InputLabelProps={{
                shrink: true,
            }}
            value={timeTo}
            onChange={(ev) => handleChange(ev, 'timeTo')}
        />
    </FormPopUp>
}

export {
    EventPopUp,
}
