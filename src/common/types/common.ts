import {ReactChild} from 'react';
type ReactChildrenArr = ReactChild[] | Element[]
type ReactChildren = ReactChild | Element | ReactChildrenArr

interface IEvent {
    target: { value: string }
}

interface IFluxAction {
    type: string
    payload?: any // tslint:disable-line no-any
    meta?: any // tslint:disable-line no-any
}
type Optional<T> = { [P in keyof T]?: T[P] }

interface INestedObj {
    [key: string]: any
}

export type {
    ReactChildrenArr,
    ReactChildren,
    IEvent,
    IFluxAction,
    Optional,
    INestedObj,
}
