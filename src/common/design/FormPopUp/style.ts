// tslint:disable-next-line:no-submodule-imports
import { makeStyles, Theme } from '@material-ui/core/styles'

const useStyles = makeStyles((theme: Theme) => ({
    popUpAction: {
        display: 'flex',
        flexDirection: 'row-reverse',
        padding: '10px',
    },
    popUpBox: {
        borderRadius: '0',
        borderWidth: '0 0 1px 0',
        flex: '1',
        padding: '20px',
        width: '100%',
    },
    popUpBtn: {
        '&:hover': {
            borderWidth: '0!important',
        },
        borderWidth: '0!important',
    },
    popUpDialog: {
        display: 'flex',
        flexDirection: 'column',
        height: '600px',
        overflow: 'auto',
        padding: '20px',
        width: '420px'
    },
    popupSubTitle: {
        fontSize: theme.typography.pxToRem(16),
        marginBottom: '20px',
    },
    popupTitle: {
        fontSize: theme.typography.pxToRem(20),
    },
}))

export { useStyles }
