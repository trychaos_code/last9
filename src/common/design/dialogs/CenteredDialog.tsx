import { Dialog, Theme } from '@material-ui/core'
// tslint:disable-next-line no-submodule-imports
import { DialogProps } from '@material-ui/core/Dialog/'
import { createStyles, withStyles, CSSProperties, WithStyles } from '@material-ui/styles'
import * as React from 'react'
const styles = (_theme: Theme) => createStyles({})

// @ts-ignore
interface ICenteredDialogProps extends WithStyles<typeof styles>, DialogProps {
    classes: CSSProperties
    children: JSX.Element
}
function CenteredDialogComp({ children, ...props }: ICenteredDialogProps): JSX.Element {
    return (
        <Dialog {...props}>
            {children}
        </Dialog>
    )
}
const CenteredDialog = withStyles(styles)(CenteredDialogComp)

export { CenteredDialog }
