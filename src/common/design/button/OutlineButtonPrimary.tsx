import { Button, ButtonProps, Theme } from '@material-ui/core'
import { createStyles, withStyles, WithStyles } from '@material-ui/styles'
import classNames from 'classnames'
import * as React from 'react'
import { ReactChildren } from '../../types'

const styles = (theme: Theme) =>
    createStyles({
        root: {
            fontWeight: 500,
        },
        leftIcon: {
            marginRight: 10,
        },
    })

export interface IOutlineButtonPrimary extends WithStyles<typeof styles> {
    className?: string
    children?: string | ReactChildren
    disabled?: boolean
    textPrimary?: boolean
    iconName?: string
    label?: string | JSX.Element
    onClick?: (...args: any[]) => void // tslint:disable-line no-any
    to?: string
}

function OutlineButtonPrimaryComp({ children, classes, className, iconName, label, to, ...props }: IOutlineButtonPrimary) {
    let buttonProps: {
        className: string
        color: string
        variant: string
    } = {
        className: classNames(className, classes.root),
        color: 'primary',
        variant: 'outlined',
    }
    buttonProps = { ...buttonProps, ...props }
    return (
        <Button {...(buttonProps as ButtonProps)}>
            {label || children}
        </Button>
    )
}

const OutlineLinkButtonPrimary = withStyles(styles)(OutlineButtonPrimaryComp)

export { OutlineLinkButtonPrimary }
