import React from 'react';
import './App.css';
import {Cal} from './cal';
import {addCustomPolyFills} from "./common/helper";

function App() {
   React.useEffect(() => addCustomPolyFills, [])
  return (
    <div className="App">
      <Cal />
    </div>
  );
}

export default App;
